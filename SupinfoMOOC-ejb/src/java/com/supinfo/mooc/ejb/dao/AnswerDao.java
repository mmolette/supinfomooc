
package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Question;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface AnswerDao {
    
    public Answer addAnswer(Answer answer);
    public void removeAnswer(Answer answer);
    public List<Answer> getAnswersByQuestion(Question question);
}
