
package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.Category;
import java.util.List;
import java.util.Set;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface CategoryDao {
    
    public Category addCategory(Category category);
    public void removeCategory(Category category);
    
    public List<Category> getAllCategories();
}
