/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import java.util.List;

/**
 *
 * @author Mathieu
 */
public interface CertificationDao {
    
    public Certification addCertification(Certification certification);
    public void removeCertification(Certification certification);
    public Certification getCertificationById(Long id);
    //public List<Certification> getCertificationsByCourse(Course course);
    public List<Certification> getCertificationsByStudent(Student student);
    //public Certification getCertificationByUserandCourse(User user, Course course);
    
}
