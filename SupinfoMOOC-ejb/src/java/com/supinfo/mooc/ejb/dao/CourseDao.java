
package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.Category;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Level;
import com.supinfo.mooc.ejb.entity.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface CourseDao {
    
    public Course addCourse(Course course);
    public void removeCourse(Course course);
    public List<Course> getAllCourses();
    public Course getCourseById(Long id);
    public List<Course> getAllCoursesByAuthor(User user);
    public List<Course> getAllCoursesByLevel(Level level);
    public List<Course> getAllCoursesByCategory(Category category);
    public List<Course> getAllCoursesByCategoryAndLevel(Category category, Level level);
}
