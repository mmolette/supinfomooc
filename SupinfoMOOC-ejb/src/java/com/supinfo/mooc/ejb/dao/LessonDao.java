
package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Lesson;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface LessonDao {
    
    public Lesson addLesson(Lesson lesson);
    public void removeLesson(Lesson lesson);
    public Lesson getLessonById(Long id);
    public List<Lesson> getAllLessonByCourse(Course course);
}
