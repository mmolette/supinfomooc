
package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.Level;
import java.util.List;
import java.util.Set;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface LevelDao {
    
    public Level addLevel(Level level);
    public void removeLevel(Level level);
    
    public List<Level> getAllLevels();
}
