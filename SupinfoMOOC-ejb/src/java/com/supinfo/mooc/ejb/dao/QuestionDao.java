
package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.Level;
import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.entity.Quizz;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface QuestionDao {
    
    public Question addQuestion(Question question);
    public void removeQuestion(Question question);
    public List<Question> getAllQuestionsByQuizz(Quizz quizz);
}
