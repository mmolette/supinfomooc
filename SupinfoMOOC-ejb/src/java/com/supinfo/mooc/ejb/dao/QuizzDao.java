
package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Quizz;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Mathieu
 */
@Local
public interface QuizzDao {
    
    public Quizz addQuizz(Quizz quizz);
    public void removeQuizz(Quizz quizz);
    public List<Quizz> getAllQuizzByCourse(Course course);
    public Quizz getQuizzById(Long id);
}
