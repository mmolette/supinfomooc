package com.supinfo.mooc.ejb.dao;

import com.supinfo.mooc.ejb.entity.User;
import javax.ejb.Local;
import javax.persistence.EntityManager;

/**
 *
 * @author Mathieu
 */
@Local
public interface UserDao {
    
    public User addUser(User user) throws Exception;
    public void removeUser(User user); 
    public User getUserByID(Long id);
    //public User getUserByToken(String  token);
    public User getUserByEmail(String email) throws Exception;
    
    public User setUser(User user);
}
