package com.supinfo.mooc.ejb.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class Answer implements Serializable
{
	 
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Long id;

	 
	@Column(nullable = false) 
	private String name;
        
        @Column(nullable = false)
        private Boolean valid;

	 
	@ManyToOne(fetch = FetchType.LAZY) 
	@JoinColumn(nullable = false) 
	private Question question;

	public Answer(){
		super();
	}


	public void basicSetQuestion(Question myQuestion) {
		if (this.question != myQuestion) {
			if (myQuestion != null){
				if (this.question != myQuestion) {
					Question oldquestion = this.question;
					this.question = myQuestion;
					if (oldquestion != null)
						oldquestion.removeAnswer(this);
				}
			}
		}	
	}
	
        
	public long getId() {
		return this.id;	
	}
	

	public String getName() {
		return this.name;	
	}
	

	public Question getQuestion() {
		return this.question;	
	}
	

	public void setId(long myId) {
		this.id = myId;	
	}
	

	public void setName(String myName) {
		this.name = myName;	
	}
	
	public void setQuestion(Question myQuestion) {
		this.basicSetQuestion(myQuestion);
		myQuestion.addAnswer(this);	
	}
	
	public void unsetId() {
		this.id = 0L;	
	}
	
	public void unsetName() {
		this.name = "";	
	}
	
	public void unsetQuestion() {
		if (this.question == null)
			return;
		Question oldquestion = this.question;
		this.question = null;
		oldquestion.removeAnswer(this);	
	}

        public Boolean isValid() {
            return valid;
        }

        public void setValid(Boolean valid) {
            this.valid = valid;
        }
        
}

