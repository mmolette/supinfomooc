package com.supinfo.mooc.ejb.entity;
import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class Author extends User implements Serializable
{
	 
	@OneToMany(mappedBy = "author", fetch = FetchType.LAZY) 
	private Set<Course> courses;

	public Author(){
		super();
	}

        @XmlTransient
	public Set<Course> getCourses() {
		if(this.courses == null) {
				this.courses = new HashSet<Course>();
		}
		return (Set<Course>) this.courses;	
	}
	
	public void addAllCourses(Set<Course> newCourse) {
		if (this.courses == null) {
			this.courses = new HashSet<Course>();
		}
		for (Course tmp : newCourse)
			tmp.setAuthor(this);
			
	}
	
	public void removeAllCourses(Set<Course> newCourse) {
		if(this.courses == null) {
			return;
		}
		
		this.courses.removeAll(newCourse);	
	}
	
	public void addCourse(Course newCourse) {
		if(this.courses == null) {
			this.courses = new HashSet<Course>();
		}
		
                if(!this.courses.contains(newCourse)){
                    if (this.courses.add(newCourse))
			newCourse.basicSetAuthor(this);	
                }
	}
	
	public void removeCourse(Course oldCourse) {
		if(this.courses == null)
			return;
		
		if (this.courses.remove(oldCourse))
			oldCourse.unsetAuthor();
			
	}
	
}

