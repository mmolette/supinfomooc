package com.supinfo.mooc.ejb.entity;
import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class Category implements Serializable
{
	 
	@Id 
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false) 
	private Long id;

	 
	@Column(nullable = false) 
	private String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	 
	@OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
	private Set<Course> courses;


	public Category(){
		super();
	}

	public long getId() {
		return this.id;	
	}
	
	public String getName() {
		return this.name;	
	}
	
        @XmlTransient
	public Set<Course> getCourses() {
		if(this.courses == null) {
				this.courses = new HashSet<Course>();
		}
		return (Set<Course>) this.courses;	
	}
	
	public void addAllCourse(Set<Course> newCourse) {
		if (this.courses == null) {
			this.courses = new HashSet<Course>();
		}
		for (Course tmp : newCourse)
			tmp.setCategory(this);
			
	}

	public void removeAllCourse(Set<Course> newCourse) {
		if(this.courses == null) {
			return;
		}
		
		this.courses.removeAll(newCourse);	
	}
	
	public void setId(long myId) {
		this.id = myId;	
	}
	
	public void setName(String myName) {
		this.name = myName;	
	}

	public void addCourse(Course newCourse) {
		if(this.courses == null) {
			this.courses = new HashSet<Course>();
		}
		
                if(!this.courses.contains(newCourse)){
                    if (this.courses.add(newCourse))
			newCourse.basicSetCategory(this);
                }
			
	}
	
	public void unsetId() {
		this.id = 0L;	
	}
	
	public void unsetName() {
		this.name = "";	
	}
	
	public void removeCourse(Course oldCourse) {
		if(this.courses == null)
			return;
		
		if (this.courses.remove(oldCourse))
			oldCourse.unsetCategory();
			
	}
	
}

