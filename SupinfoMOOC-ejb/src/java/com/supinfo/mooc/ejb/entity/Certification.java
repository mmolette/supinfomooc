package com.supinfo.mooc.ejb.entity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class Certification implements Serializable
{
	 
	@Id 
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false) 
	private Long id;

	 
	@Temporal(javax.persistence.TemporalType.DATE) 
	@Column(nullable = false) 
	private Date date = new Date();

	 
	@ManyToOne(fetch = FetchType.LAZY) 
	@JoinColumn(nullable = false) 
	private Student student;
        
        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name="COURSE_ID", nullable = false)
        private Course course;


	public Certification(){
		super();
	}


	public void basicSetStudent(Student myStudent) {
		if (this.student != myStudent) {
			if (myStudent != null){
				if (this.student != myStudent) {
					Student oldstudent = this.student;
					this.student = myStudent;
					if (oldstudent != null)
						oldstudent.removeCertification(this);
				}
			}
		}	
	}
	
	public long getId() {
		return this.id;	
	}
	
	public Date getDate() {
		return this.date;	
	}
	
	public Student getStudent() {
		return this.student;	
	}
	
	public void setId(long myId) {
		this.id = myId;	
	}
	
	public void setDate(Date myDate) {
		this.date = myDate;	
	}
	
	public void setStudent(Student myStudent) {
		this.basicSetStudent(myStudent);
		myStudent.addCertification(this);	
	}
	
	public void unsetId() {
		this.id = 0L;	
	}
	
	public void unsetDate() {
		this.date = new Date();	
	}
	
	public void unsetStudent() {
		if (this.student == null)
			return;
		Student oldstudent = this.student;
		this.student = null;
		oldstudent.removeCertification(this);	
	}

        public Course getCourse() {
            return course;
        }

        public void setCourse(Course course) {
            this.course = course;
        }
        
        
	
}

