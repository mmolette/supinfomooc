package com.supinfo.mooc.ejb.entity;
import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity
@XmlRootElement
public class Course implements Serializable
{

	 
	@Id 
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false) 
	private Long id;

	 
	@Column(nullable = false) 
	private String name;

	 
	@Column(nullable = false) 
	private String description;

	 
	@Column(nullable = false) 
	private int duration;

	 
	@Temporal(javax.persistence.TemporalType.DATE) 
	@Column(nullable = false) 
	private Date created;

	 
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = false) 
	private Author author;

	 
	@OneToOne(mappedBy = "course", fetch = FetchType.LAZY) 
	private Quizz quizz;

	 
	@OneToMany(mappedBy = "course", fetch = FetchType.LAZY) 
	private Set<Lesson> lessons;

	 
	@ManyToMany(mappedBy = "courses", fetch = FetchType.LAZY) 
	private Set<Student> students;

	 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false) 
	private Category category;
	 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false) 
	private Level level;


	public Course(){
		super();
	}

	public void basicSetAuthor(Author myAuthor) {
		if (this.author != myAuthor) {
			if (myAuthor != null){
				if (this.author != myAuthor) {
					Author oldauthor = this.author;
					this.author = myAuthor;
					if (oldauthor != null)
						oldauthor.removeCourse(this);
				}
			}
		}	
	}
	
	public void basicSetQuizz(Quizz myQuizz) {
		if (this.quizz != myQuizz) {
			if (myQuizz != null){
				if (this.quizz != myQuizz) {
					Quizz oldquizz = this.quizz;
					this.quizz = myQuizz;
					if (oldquizz != null)
						oldquizz.unsetCourse();
				}
			}
		}	
	}
	

	public void basicSetCategory(Category myCategory) {
		if (this.category != myCategory) {
			if (myCategory != null){
				if (this.category != myCategory) {
					Category oldcategory = this.category;
					this.category = myCategory;
					if (oldcategory != null)
						oldcategory.removeCourse(this);
				}
			}
		}	
	}
	
	public void basicSetLevel(Level myLevel) {
		if (this.level != myLevel) {
			if (myLevel != null){
				if (this.level != myLevel) {
					Level oldlevel = this.level;
					this.level = myLevel;
					if (oldlevel != null)
						oldlevel.removeCourse(this);
				}
			}
		}	
	}
	
	public long getId() {
		return this.id;	
	}
	
	public String getName() {
		return this.name;	
	}
	
	public String getDescription() {
		return this.description;	
	}
	
	public int getDuration() {
		return this.duration;	
	}
	
	public Date getCreated() {
		return this.created;	
	}
	
	public Author getAuthor() {
		return this.author;	
	}
	
	public Quizz getQuizz() {
		return this.quizz;	
	}
	
        @XmlTransient
	public Set<Lesson> getLessons() {
		if(this.lessons == null) {
				this.lessons = new HashSet<Lesson>();
		}
		return (Set<Lesson>) this.lessons;	
	}
	
        @XmlTransient
	public Set<Student> getStudents() {
		if(this.students == null) {
				this.students = new HashSet<Student>();
		}
		return (Set<Student>) this.students;	
	}
	
	public Category getCategory() {
		return this.category;	
	}
	
	public Level getLevel() {
		return this.level;	
	}
	
	public void addAllLessons(Set<Lesson> newLesson) {
		if (this.lessons == null) {
			this.lessons = new HashSet<Lesson>();
		}
		for (Lesson tmp : newLesson)
			tmp.setCourse(this);
			
	}
	
	public void addAllStudents(Set<Student> newStudent) {
		if (this.students == null) {
			this.students = new HashSet<Student>();
		}
		for (Student tmp : newStudent)
			tmp.addCourse(this);
			
	}
	
	public void removeAllLessons(Set<Lesson> newLesson) {
		if(this.lessons == null) {
			return;
		}
		
		this.lessons.removeAll(newLesson);	
	}
	
	public void removeAllStudents(Set<Student> newStudent) {
		if(this.students == null) {
			return;
		}
		
		this.students.removeAll(newStudent);	
	}
	
	public void setId(long myId) {
		this.id = myId;	
	}
	
	public void setName(String myName) {
		this.name = myName;	
	}
	
	public void setDescription(String myDescription) {
		this.description = myDescription;	
	}
	
	public void setDuration(int myDuration) {
		this.duration = myDuration;	
	}
	
	public void setCreated(Date myCreated) {
		this.created = myCreated;	
	}
	
	public void setAuthor(Author myAuthor) {
		this.basicSetAuthor(myAuthor);
		myAuthor.addCourse(this);	
	}
	
	public void setQuizz(Quizz myQuizz) {
		this.basicSetQuizz(myQuizz);
		myQuizz.basicSetCourse(this);
			
	}
	
	public void addLesson(Lesson newLesson) {
		if(this.lessons == null) {
			this.lessons = new HashSet<Lesson>();
		}
		
                if(!this.lessons.contains(newLesson)){
                    if (this.lessons.add(newLesson))
			newLesson.basicSetCourse(this);
                }
			
	}
	
	public void addStudent(Student newStudent) {
		if(this.students == null) {
			this.students = new HashSet<Student>();
		}
		
		if (this.students.add(newStudent)){
                    if(!newStudent.getCourses().contains(this)){
                        newStudent.addCourse(this);	
                    }
                }
                    
			
	}
	
	public void setCategory(Category myCategory) {
		this.basicSetCategory(myCategory);
		myCategory.addCourse(this);	
	}
	
	public void setLevel(Level myLevel) {
		this.basicSetLevel(myLevel);
		myLevel.addCourse(this);	
	}
	
	public void unsetId() {
		this.id = 0L;	
	}
	
	public void unsetName() {
		this.name = "";	
	}
	
	public void unsetDescription() {
		this.description = "";	
	}
	
	public void unsetDuration() {
		this.duration = 0;	
	}
	
	public void unsetCreated() {
		this.created = new Date();	
	}
	
	public void unsetAuthor() {
		if (this.author == null)
			return;
		Author oldauthor = this.author;
		this.author = null;
		oldauthor.removeCourse(this);	
	}
	
	public void unsetQuizz() {
		if (this.quizz == null)
			return;
		Quizz oldquizz = this.quizz;
		this.quizz = null;
		oldquizz.unsetCourse();	
	}
	
	public void removeLesson(Lesson oldLesson) {
		if(this.lessons == null)
			return;
		
		if (this.lessons.remove(oldLesson))
			oldLesson.unsetCourse();
			
	}
	
	public void removeStudent(Student oldStudent) {
		if(this.students == null)
			return;
		
		if (this.students.remove(oldStudent))
			oldStudent.removeCourse(this);
			
	}
	
	public void unsetCategory() {
		if (this.category == null)
			return;
		Category oldcategory = this.category;
		this.category = null;
		oldcategory.removeCourse(this);	
	}
	
	public void unsetLevel() {
		if (this.level == null)
			return;
		Level oldlevel = this.level;
		this.level = null;
		oldlevel.removeCourse(this);	
	}

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 53 * hash + Objects.hashCode(this.id);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Course other = (Course) obj;
            if (!Objects.equals(this.id, other.id)) {
                return false;
            }
            return true;
        }	
}

