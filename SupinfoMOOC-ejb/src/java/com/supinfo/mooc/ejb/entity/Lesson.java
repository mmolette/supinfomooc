package com.supinfo.mooc.ejb.entity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class Lesson implements Serializable
{
	 
	@Id 
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false) 
	private Long id;

	 
	@Column(nullable = false) 
	private String name;

	 
	@Column(nullable = false) 
	private String description;
        
        @Column(nullable = false) 
	private String content;

	 
	@Column(nullable = false) 
	private int duration;

	 
	@Temporal(javax.persistence.TemporalType.DATE) 
	@Column(nullable = false) 
	private Date created;

	 
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false) 
	private Course course;

	public Lesson(){
		super();
	}

	public void basicSetCourse(Course myCourse) {
		if (this.course != myCourse) {
			if (myCourse != null){
				if (this.course != myCourse) {
					Course oldcourse = this.course;
					this.course = myCourse;
					if (oldcourse != null)
						oldcourse.removeLesson(this);
				}
			}
		}	
	}
	
	public long getId() {
		return this.id;	
	}
	
	public String getName() {
		return this.name;	
	}
	
	public String getDescription() {
		return this.description;	
	}
        
        public String getContent() {
		return this.content;	
	}
	
	public int getDuration() {
		return this.duration;	
	}
	
	public Date getCreated() {
		return this.created;	
	}
	
	public Course getCourse() {
		return this.course;	
	}
	
	public void setId(long myId) {
		this.id = myId;	
	}
	
	public void setName(String myName) {
		this.name = myName;	
	}
	
	public void setDescription(String myDescription) {
		this.description = myDescription;	
	}
        
        public void setContent(String content) {
		this.content = content;	
	}
	
	public void setDuration(int myDuration) {
		this.duration = myDuration;	
	}
	
	public void setCreated(Date myCreated) {
		this.created = myCreated;	
	}
	
	public void setCourse(Course myCourse) {
		this.basicSetCourse(myCourse);
		myCourse.addLesson(this);	
	}
	
	public void unsetId() {
		this.id = 0L;	
	}
	
	public void unsetName() {
		this.name = "";	
	}
	
	public void unsetDescription() {
		this.description = "";	
	}
	
	public void unsetDuration() {
		this.duration = 0;	
	}
	
	public void unsetCreated() {
		this.created = new Date();	
	}
	
	public void unsetCourse() {
		if (this.course == null)
			return;
		Course oldcourse = this.course;
		this.course = null;
		oldcourse.removeLesson(this);	
	}
	
}

