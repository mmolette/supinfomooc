package com.supinfo.mooc.ejb.entity;
import java.util.Set;
import java.util.HashSet;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class Question
{
	 
	@Id 
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false) 
	private Long id;

	 
	@Column(nullable = false) 
	private String name;

	 
	@ManyToOne(fetch = FetchType.LAZY) 
	@JoinColumn(nullable = false) 
	private Quizz quizz;

	 
	@OneToMany(fetch = FetchType.LAZY)
	private Set<Answer> answers;


	public Question(){
		super();
	}

	public void basicSetQuizz(Quizz myQuizz) {
		if (this.quizz != myQuizz) {
			if (myQuizz != null){
				if (this.quizz != myQuizz) {
					Quizz oldquizz = this.quizz;
					this.quizz = myQuizz;
					if (oldquizz != null)
						oldquizz.removeQuestion(this);
				}
			}
		}	
	}
	
	public long getId() {
		return this.id;	
	}
	
	public String getName() {
		return this.name;	
	}
	
	public Quizz getQuizz() {
		return this.quizz;	
	}
	
        @XmlTransient
	public Set<Answer> getAnswers() {
		if(this.answers == null) {
				this.answers = new HashSet<Answer>();
		}
		return (Set<Answer>) this.answers;	
	}
	
	public void addAllAnswers(Set<Answer> newAnswer) {
		if (this.answers == null) {
			this.answers = new HashSet<Answer>();
		}
		for (Answer tmp : newAnswer)
			tmp.setQuestion(this);
			
	}
	
	public void removeAllAnswers(Set<Answer> newAnswer) {
		if(this.answers == null) {
			return;
		}
		
		this.answers.removeAll(newAnswer);	
	}
	
	public void setId(long myId) {
		this.id = myId;	
	}
	
	public void setName(String myName) {
		this.name = myName;	
	}
	
	public void setQuizz(Quizz myQuizz) {
		this.basicSetQuizz(myQuizz);
		myQuizz.addQuestion(this);	
	}

	public void addAnswer(Answer newAnswer) {
		if(this.answers == null) {
			this.answers = new HashSet<Answer>();
		}
		
                if(!this.answers.contains(newAnswer)){
                    if (this.answers.add(newAnswer))
			newAnswer.basicSetQuestion(this);
                }
			
	}
	
	public void unsetId() {
		this.id = 0L;	
	}
	
	public void unsetName() {
		this.name = "";	
	}
	
	public void unsetQuizz() {
		if (this.quizz == null)
			return;
		Quizz oldquizz = this.quizz;
		this.quizz = null;
		oldquizz.removeQuestion(this);	
	}
	
	public void removeAnswer(Answer oldAnswer) {
		if(this.answers == null)
			return;
		
		if (this.answers.remove(oldAnswer))
			oldAnswer.unsetQuestion();
			
	}
	
}

