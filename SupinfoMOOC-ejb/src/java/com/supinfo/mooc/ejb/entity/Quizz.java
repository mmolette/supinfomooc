package com.supinfo.mooc.ejb.entity;
import java.io.Serializable;
import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class Quizz implements Serializable
{
	 
	@Id 
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false) 
	private Long id;

	 
	@Column(nullable = false) 
	private String name;

	 
	@Column(nullable = false) 
	private String description;

	 
	@Column(nullable = false) 
	private int duration;

	 
	@Temporal(javax.persistence.TemporalType.DATE) 
	@Column(nullable = false) 
	private Date created;

	 
	@OneToMany(fetch = FetchType.LAZY)
	private Set<Question> questions;

	 
	@OneToOne(fetch = FetchType.LAZY)
	private Course course;

	public Quizz(){
		super();
	}

	public void basicSetCourse(Course myCourse) {
		if (this.course != myCourse) {
			if (myCourse != null){
				if (this.course != myCourse) {
					Course oldcourse = this.course;
					this.course = myCourse;
					if (oldcourse != null)
						oldcourse.unsetQuizz();
				}
			}
		}	
	}
	
	public long getId() {
		return this.id;	
	}
	
	public String getName() {
		return this.name;	
	}

	public String getDescription() {
		return this.description;	
	}
	
	public int getDuration() {
		return this.duration;	
	}

	public Date getCreated() {
		return this.created;	
	}
	
        @XmlTransient
	public Set<Question> getQuestions() {
		if(this.questions == null) {
				this.questions = new HashSet<Question>();
		}
		return (Set<Question>) this.questions;	
	}
	
        @XmlTransient
	public Course getCourse() {
		return this.course;	
	}
	
	public void addAllQuestions(Set<Question> newQuestion) {
		if (this.questions == null) {
			this.questions = new HashSet<Question>();
		}
		for (Question tmp : newQuestion)
			tmp.setQuizz(this);
			
	}
	
	public void removeAllQuestions(Set<Question> newQuestion) {
		if(this.questions == null) {
			return;
		}
		
		this.questions.removeAll(newQuestion);	
	}
	
	public void setId(long myId) {
		this.id = myId;	
	}
	
	public void setName(String myName) {
		this.name = myName;	
	}
	
	public void setDescription(String myDescription) {
		this.description = myDescription;	
	}
	
	public void setDuration(int myDuration) {
		this.duration = myDuration;	
	}

	public void setCreated(Date myCreated) {
		this.created = myCreated;	
	}

	public void addQuestion(Question newQuestion) {
		if(this.questions == null) {
			this.questions = new HashSet<Question>();
		}
		
                if(!this.questions.contains(newQuestion)){
                    if (this.questions.add(newQuestion))
			newQuestion.basicSetQuizz(this);
                }
			
	}
	
	public void setCourse(Course myCourse) {
		this.basicSetCourse(myCourse);
		myCourse.basicSetQuizz(this);
			
	}
	
	public void unsetId() {
		this.id = 0L;	
	}

	public void unsetName() {
		this.name = "";	
	}
	
	public void unsetDescription() {
		this.description = "";	
	}
	
	public void unsetDuration() {
		this.duration = 0;	
	}
	
	public void unsetCreated() {
		this.created = new Date();	
	}
	
	public void removeQuestion(Question oldQuestion) {
		if(this.questions == null)
			return;
		
		if (this.questions.remove(oldQuestion))
			oldQuestion.unsetQuizz();
			
	}
	
	public void unsetCourse() {
		if (this.course == null)
			return;
		Course oldcourse = this.course;
		this.course = null;
		oldcourse.unsetQuizz();	
	}
	
}

