package com.supinfo.mooc.ejb.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class Student extends User implements Serializable
{
	 
	@OneToMany(fetch = FetchType.LAZY) 
	private List<Certification> certifications;
	 
	@ManyToMany(fetch = FetchType.LAZY)
	private List<Course> courses;

	public Student(){
		super();
	}
        
        public Student(String email, String lastname, String firstname, String password){
            super(email, lastname, firstname, password);
        }

        @XmlTransient
	public List<Certification> getCertifications() {
            if(this.certifications == null) {
                            this.certifications = new ArrayList<Certification>();
            }
            return (List<Certification>) this.certifications;	
	}
	
        @XmlTransient
	public List<Course> getCourses() {
            if(this.courses == null) {
                            this.courses = new ArrayList<Course>();
            }
            return (List<Course>) this.courses;	
	}
	
	public void addAllCertifications(List<Certification> newCertification) {
            if (this.certifications == null) {
                    this.certifications = new ArrayList<Certification>();
            }
            for (Certification tmp : newCertification)
                    tmp.setStudent(this);
			
	}
	
	public void addAllCourses(List<Course> newCourse) {
            if (this.courses == null) {
                    this.courses = new ArrayList<Course>();
            }
            for (Course tmp : newCourse)
                    tmp.addStudent(this);
			
	}

	public void removeAllCertifications(List<Certification> newCertification) {
            if(this.certifications == null) {
                    return;
            }

            this.certifications.removeAll(newCertification);	
	}
	
	public void removeAllCourses(List<Course> newCourse) {
            if(this.courses == null) {
                    return;
            }

            this.courses.removeAll(newCourse);	
	}
	
	public void addCertification(Certification newCertification) {
		if(this.certifications == null) {
			this.certifications = new ArrayList<Certification>();
		}
		
                if(!this.certifications.contains(newCertification)){
                    if (this.certifications.add(newCertification))
			newCertification.basicSetStudent(this);	
                }
		
	}
	
	public void addCourse(Course newCourse) {
            if(this.courses == null) {
                    this.courses = new ArrayList<Course>();
            }

            if (this.courses.add(newCourse)){
                if(!newCourse.getStudents().contains(this)){
                    newCourse.addStudent(this);
                }
            }       	
	}
	
	public void removeCertification(Certification oldCertification) {
            if(this.certifications == null)
                    return;

            if (this.certifications.remove(oldCertification))
                    oldCertification.unsetStudent();
			
	}
	
	public void removeCourse(Course oldCourse) {
            if(this.courses == null)
                    return;

            if (this.courses.remove(oldCourse))
                    oldCourse.removeStudent(this);
			
	}
	
}

