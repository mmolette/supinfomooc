package com.supinfo.mooc.ejb.entity;
import com.supinfo.mooc.util.Hash;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */
 
@Entity 
@XmlRootElement
public class User implements Serializable
{

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false) 
    private Long id;


    @Column(nullable = false) 
    private String email;


    @Column(nullable = false) 
    private String password;


    @Column(nullable = false) 
    private String lastname;


    @Column(nullable = false) 
    private String firstname;


    @Temporal(javax.persistence.TemporalType.DATE) 
    @Column(nullable = false) 
    private Date registered;


    @Temporal(javax.persistence.TemporalType.DATE) 
    @Column(nullable = false) 
    private Date lastLogin;

    @Column(nullable = false) 
    private Boolean valid = false;

    public User(){
            super();
    }

    public User(String email, String lastname, String firstname, String password){
        super();
        this.email = email;
        this.lastname = lastname;
        this.firstname = firstname;
        this.lastLogin = new Date();
        this.registered = new Date();

        Hash hash = new Hash();
        this.password = hash.hash("SHA1", password);
        //this.token = hash.hash("SHA1", email);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Boolean isValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
	
    
}

