/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.dao.AnswerDao;
import com.supinfo.mooc.ejb.entity.Answer_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class AnswerFacade extends AbstractFacade<Answer> implements AnswerDao{
    
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AnswerFacade() {
        super(Answer.class);
    }

    @Override
    public Answer addAnswer(Answer answer) {
        em.persist(answer);
        return answer;
    }

    @Override
    public void removeAnswer(Answer answer) {
        em.remove(answer);
    }

    @Override
    public List<Answer> getAnswersByQuestion(Question question) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Answer> query = cb.createQuery(Answer.class);
        Root<Answer> answer = query.from(Answer.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(answer.get(Answer_.question), question));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
        /*
        Query query = em.createQuery("SELECT A FROM Answer as A " +
                                    "WHERE A.question = :question ");
        query.setParameter("question", question);
        */
        //return query.getResultList();
    }
    
}
