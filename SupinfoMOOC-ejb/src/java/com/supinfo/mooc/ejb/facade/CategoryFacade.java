/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.entity.Category;
import com.supinfo.mooc.ejb.dao.CategoryDao;
import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Answer_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class CategoryFacade extends AbstractFacade<Category> implements CategoryDao{
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategoryFacade() {
        super(Category.class);
    }

    @Override
    public Category addCategory(Category category) {
        em.persist(category);
        return category;
    }

    @Override
    public void removeCategory(Category category) {
        em.remove(category);
    }

    @Override
    public List<Category> getAllCategories() {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Category> query = cb.createQuery(Category.class);

        return em.createQuery(query).getResultList();
        
        //return em.createQuery("SELECT c FROM Category c").getResultList();
    }
    
}
