/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.dao.CertificationDao;
import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.Certification_;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Lesson;
import com.supinfo.mooc.ejb.entity.Lesson_;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class CertificationFacade extends AbstractFacade<Certification> implements CertificationDao {
    
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CertificationFacade() {
        super(Certification.class);
    }

    @Override
    public Certification addCertification(Certification certification) {
        em.persist(certification);
        return certification;
    }

    @Override
    public void removeCertification(Certification certification) {
        em.remove(certification);
    }

    @Override
    public Certification getCertificationById(Long id) {
        
        Certification certification = em.find(Certification.class, id);
        return certification;
    }

    /*
    @Override
    public List<Certification> getCertificationsByCourse(Course course) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Certification> query = cb.createQuery(Certification.class);
        Root<Certification> certification = query.from(Certification.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(certification.get(Certification_.course), course));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
    }
    */

    @Override
    public List<Certification> getCertificationsByStudent(Student student) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Certification> query = cb.createQuery(Certification.class);
        Root<Certification> certification = query.from(Certification.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(certification.get(Certification_.student), student));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
    }

    /*
    @Override
    public Certification getCertificationByUserandCourse(User user, Course course) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Certification> query = cb.createQuery(Certification.class);
        Root<Certification> certification = query.from(Certification.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(certification.get(Certification_.course), course));
        predicates.add(cb.equal(certification.get(Certification_.user), user));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getSingleResult();
    }
    */
    
}
