/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.entity.Category;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Level;
import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.dao.CourseDao;
import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Answer_;
import com.supinfo.mooc.ejb.entity.Course_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class CourseFacade extends AbstractFacade<Course> implements CourseDao{
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CourseFacade() {
        super(Course.class);
    }

    @Override
    public Course addCourse(Course course) {
        em.persist(course);
        return course;
    }

    @Override
    public void removeCourse(Course course) {
        em.remove(course);
    }

    @Override
    public List<Course> getAllCourses() {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Course> query = cb.createQuery(Course.class);

        return em.createQuery(query).getResultList();
        
        //return em.createQuery("SELECT C FROM Course as C ").getResultList();
    }
    
    @Override
    public Course getCourseById(Long id) {
        
        Course course = em.find(Course.class, id);
        if(course != null){
            course.getLessons().size();
            course.getStudents().size();
        }

        return course;
    }

    @Override
    public List<Course> getAllCoursesByLevel(Level level) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Course> query = cb.createQuery(Course.class);
        Root<Course> course = query.from(Course.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(course.get(Course_.level), level));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
        
        /*
        Query query = em.createQuery("SELECT C FROM Course as C " +
                        "WHERE C.level = :level  ");
        query.setParameter("level", level);
        return query.getResultList();
                */
    }

    @Override
    public List<Course> getAllCoursesByCategory(Category category) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Course> query = cb.createQuery(Course.class);
        Root<Course> course = query.from(Course.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(course.get(Course_.category), category));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
        
        /*
        Query query = em.createQuery("SELECT C FROM Course as C " +
                        "WHERE C.category = :category  ");
        query.setParameter("category", category);
        return query.getResultList();
                */
    }

    @Override
    public List<Course> getAllCoursesByCategoryAndLevel(Category category, Level level) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Course> query = cb.createQuery(Course.class);
        Root<Course> course = query.from(Course.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(course.get(Course_.category), category));
        predicates.add(cb.equal(course.get(Course_.level), level));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
        
        /*
        Query query = em.createQuery("SELECT C FROM Course as C " +
                        "WHERE C.category = :category  " +
                        " AND C.level = :level ");
        query.setParameter("category", category);
        query.setParameter("level", level);
        return query.getResultList();
                */
    }

    @Override
    public List<Course> getAllCoursesByAuthor(User user) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Course> query = cb.createQuery(Course.class);
        Root<Course> course = query.from(Course.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(course.get(Course_.author), user));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
        
        /*
        Query query = em.createQuery("SELECT C FROM Course as C " +
                        "WHERE C.user = :user  ");
        query.setParameter("user", user);
        return query.getResultList();
                */
    }
    
}
