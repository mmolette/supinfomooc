/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Lesson;
import com.supinfo.mooc.ejb.dao.LessonDao;
import com.supinfo.mooc.ejb.entity.Course_;
import com.supinfo.mooc.ejb.entity.Lesson_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class LessonFacade extends AbstractFacade<Lesson> implements LessonDao{
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LessonFacade() {
        super(Lesson.class);
    }

    @Override
    public Lesson addLesson(Lesson lesson) {
        em.persist(lesson);
        return lesson;
    }

    @Override
    public void removeLesson(Lesson lesson) {
        em.remove(lesson);
    }

    @Override
    public List<Lesson> getAllLessonByCourse(Course course) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Lesson> query = cb.createQuery(Lesson.class);
        Root<Lesson> lesson = query.from(Lesson.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(lesson.get(Lesson_.course), course));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
        
        /*
        Query query = em.createQuery("SELECT L FROM Lesson as L " +
                        "WHERE L.course = :course  ");
        query.setParameter("course", course);
        return query.getResultList();
                */
    }

    @Override
    public Lesson getLessonById(Long id) {
        
        return em.find(Lesson.class, id);
    }
    
}
