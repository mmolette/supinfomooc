/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.entity.Level;
import com.supinfo.mooc.ejb.dao.LevelDao;
import com.supinfo.mooc.ejb.entity.Lesson;
import com.supinfo.mooc.ejb.entity.Lesson_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class LevelFacade extends AbstractFacade<Level> implements LevelDao{
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LevelFacade() {
        super(Level.class);
    }

    @Override
    public Level addLevel(Level level) {
        em.persist(level);
        return level;
    }

    @Override
    public void removeLevel(Level level) {
        em.remove(level);
    }

    @Override
    public List<Level> getAllLevels() {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Level> query = cb.createQuery(Level.class);

        return em.createQuery(query).getResultList();
    }
    
}
