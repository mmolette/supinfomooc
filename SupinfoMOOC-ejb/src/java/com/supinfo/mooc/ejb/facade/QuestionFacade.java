/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.entity.Quizz;
import com.supinfo.mooc.ejb.dao.QuestionDao;
import com.supinfo.mooc.ejb.entity.Lesson;
import com.supinfo.mooc.ejb.entity.Lesson_;
import com.supinfo.mooc.ejb.entity.Question_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class QuestionFacade extends AbstractFacade<Question> implements QuestionDao{
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public QuestionFacade() {
        super(Question.class);
    }

    @Override
    public Question addQuestion(Question question) {
        em.persist(question);
        return question;
    }

    @Override
    public void removeQuestion(Question question) {
        em.remove(question);
    }

    @Override
    public List<Question> getAllQuestionsByQuizz(Quizz quizz) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Question> query = cb.createQuery(Question.class);
        Root<Question> question = query.from(Question.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(question.get(Question_.quizz), quizz));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
        
        /*
        Query query = em.createQuery("SELECT Q FROM Question as Q " +
                                    "WHERE Q.Quizz = :quizz ");
        query.setParameter("quizz", quizz);
        return query.getResultList();
                */
    }
    
}
