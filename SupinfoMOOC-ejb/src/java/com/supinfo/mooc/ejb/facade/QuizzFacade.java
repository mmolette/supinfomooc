/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Quizz;
import com.supinfo.mooc.ejb.dao.QuizzDao;
import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.entity.Question_;
import com.supinfo.mooc.ejb.entity.Quizz_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class QuizzFacade extends AbstractFacade<Quizz> implements QuizzDao{
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public QuizzFacade() {
        super(Quizz.class);
    }

    @Override
    public Quizz addQuizz(Quizz quizz) {
        em.persist(quizz);
        return quizz;
    }

    @Override
    public void removeQuizz(Quizz quizz) {
        em.remove(quizz);
    }

    @Override
    public List<Quizz> getAllQuizzByCourse(Course course) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Quizz> query = cb.createQuery(Quizz.class);
        Root<Quizz> quizz = query.from(Quizz.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(quizz.get(Quizz_.course), course));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getResultList();
        
        /*
        Query query = em.createQuery("SELECT Q FROM Quizz as Q " +
                        "WHERE Q.course = :course  ");
        query.setParameter("course", course);
        return query.getResultList();
                */
    }

    @Override
    public Quizz getQuizzById(Long id) {
        
        Quizz quizz = em.find(Quizz.class, id);
        if(quizz != null){
            quizz.getQuestions().size();
        }
        
        return quizz;
    }
    
}
