/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.facade;

import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.dao.UserDao;
import com.supinfo.mooc.ejb.entity.Author;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User_;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Mathieu
 */
@Stateless
public class UserFacade extends AbstractFacade<User> implements UserDao{
    @PersistenceContext(unitName = "SupinfoMOOC-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    @Override
    public User addUser(User user) throws Exception{
        try {
            em.persist(user);
            em.flush();
        }
        catch(PersistenceException pe){
            
            if(pe.getCause().getClass().getName().compareTo("org.eclipse.persistence.exceptions.DatabaseException") == 0){
		throw new Exception("This email is already used !", new Throwable("EmailDuplicateException"));
            }
        }
        catch(Exception e){
            throw e;
        }
        
        return user;
    }

    @Override
    public void removeUser(User user) {
        em.remove(user);
    }

    @Override
    public User getUserByID(Long id) {
        
        return em.find(User.class, id);
    }

    /*
    @Override
    public User getUserByToken(String token) {
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(user.get(User_.token), token));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        return em.createQuery(query).getSingleResult();
        
        /*
        Query query = em.createQuery("SELECT U FROM User as U " +
                                    "WHERE U.token = :token");
        query.setParameter("token", token);
        
        return (User)query.getSingleResult();
                
    }
*/
    
    @Override
    public User getUserByEmail(String email) throws Exception{
        
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> user = query.from(User.class);
        
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(user.get(User_.email), email));
        query.where(predicates.toArray(new Predicate[predicates.size()]));

        try{
            User u = em.createQuery(query).getSingleResult();
            if(u != null){
                if(u instanceof Student){
                    Student s = (Student) u;
                    s.getCertifications().size();
                    s.getCourses().size();
                }else{
                    if(u instanceof Author){
                        Author a = (Author) u;
                        a.getCourses().size();
                    }
                }
            }
            
            return u;
        }catch(PersistenceException pe){
            throw new Exception("This account doesn't exist !", pe.getCause());
        }
        
        
        /*
        Query query = em.createQuery("SELECT U FROM User as U " +
                                    "WHERE U.email = :email");
        query.setParameter("email", email);
        
        return (User)query.getSingleResult();
                */
    }

    @Override
    public User setUser(User user) {
        
        return em.merge(user);
    }
}
