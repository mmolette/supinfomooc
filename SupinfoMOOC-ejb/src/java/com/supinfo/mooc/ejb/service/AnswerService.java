/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.AnswerDao;
import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Question;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class AnswerService {
    
    @EJB
    private AnswerDao answerDao;
    
    public Answer addAnswer(Answer answer){
        return answerDao.addAnswer(answer);
    }
    
    public void removeAnswer(Answer answer){
        answerDao.removeAnswer(answer);
    }
    
    public List<Answer> getAnswersByQuestion(Question question){
        return answerDao.getAnswersByQuestion(question);
    }
}
