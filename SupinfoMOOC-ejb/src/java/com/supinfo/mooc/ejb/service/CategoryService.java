/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.CategoryDao;
import com.supinfo.mooc.ejb.entity.Category;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class CategoryService {
    
    @EJB
    private CategoryDao categoryDao;
    
    public Category addCategory(Category category){
        return categoryDao.addCategory(category);
    }
    public void removeCategory(Category category){
        categoryDao.removeCategory(category);
    }
    
    public List<Category> getAllCategories(){
        return categoryDao.getAllCategories();
    }
}
