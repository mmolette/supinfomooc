/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.CertificationDao;
import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.Student;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author Mathieu
 */
@Stateless
public class CertificationService {
    
    @EJB
    CertificationDao certificationDao;
    
    @Resource(mappedName = "jms/queue/supmoocQueueDestinationFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName="jms/queue/supmoocQueueDestination")
    private Queue printerQueue;
    
    public Certification addCertification(Certification certification){
        return certificationDao.addCertification(certification);
    }
    
    public void removeCertification(Certification certification){
        certificationDao.removeCertification(certification);
    }
    
    public Certification getCertificationById(Long id){
        return certificationDao.getCertificationById(id);
    }
    
    /*
    public List<Certification> getCertificationsByCourse(Course course){
        return certificationDao.getCertificationsByCourse(course);
    }
    */
    
    public List<Certification> getCertificationsByStudent(Student student){
        return certificationDao.getCertificationsByStudent(student);
    }
    
    /*
    public Certification getCertificationByUserandCourse(User user, Course course){
        return certificationDao.getCertificationByUserandCourse(user, course);
    }
    */
    public void printCertification(Certification certification){
        this.sendMessage(certification);
    }
    
    private void sendMessage(Certification certification) {
        Connection connection = null;
        Session session = null;

        try {
            connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create a MessageProducer from the Session to the Topic or Queue
            MessageProducer producer = session.createProducer(printerQueue);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            // Create a message
            
            StringBuilder sb = new StringBuilder();
            sb.append("Certification : \n");
            sb.append("\nFirstname : ");
            sb.append(certification.getStudent().getFirstname());
            sb.append("\nLastname : ");
            sb.append(certification.getStudent().getLastname());
            sb.append("\nCourse : ");
            sb.append(certification.getCourse().getName());
            sb.append("\nCourse : ");
            sb.append(certification.getDate());

            TextMessage message = session.createTextMessage(sb.toString());

            // Tell the producer to send the message
            producer.send(message);
            
            System.out.println("message sent!");
        } catch (JMSException ex) {
            Logger.getLogger(CertificationService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                // Clean up
                if (session != null) session.close();
                if (connection != null) connection.close();
            } catch (JMSException ex) {
                Logger.getLogger(CertificationService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
