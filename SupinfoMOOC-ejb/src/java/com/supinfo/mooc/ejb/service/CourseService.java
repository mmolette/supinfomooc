/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.CourseDao;
import com.supinfo.mooc.ejb.entity.Category;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Level;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import static java.rmi.server.LogStream.log;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class CourseService {
   
    @EJB
    private CourseDao courseDao;
    
    @EJB
    private UserService userService;
    
    public Course addCourse(Course course){
        return courseDao.addCourse(course);
    }
    
    public void removeCourse(Course course){
        courseDao.removeCourse(course);
    }
    
    public List<Course> getAllCourses(){
        return courseDao.getAllCourses();
    }
    
    public Course getCourseById(Long id){
        return courseDao.getCourseById(id);
    }
    
    public List<Course> getAllCoursesByUser(User user){
        return courseDao.getAllCoursesByAuthor(user);
    }
    
    public List<Course> getAllCoursesByLevel(Level level){
        return courseDao.getAllCoursesByLevel(level);
    }
    
    public List<Course> getAllCoursesByCategory(Category category){
        return courseDao.getAllCoursesByCategory(category);
    }
    
    public List<Course> getAllCoursesByCategoryAndLevel(Category category, Level level){
        return courseDao.getAllCoursesByCategoryAndLevel(category, level);
    }
    
    public void followCourse(Student student, Course course){
        
        student.addCourse(course);
        try {
            student = (Student) userService.mergeUser(student);
        } catch (Exception e) {
            log(e.getMessage());
        }
    }
}
