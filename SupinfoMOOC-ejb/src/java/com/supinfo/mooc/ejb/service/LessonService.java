/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.LessonDao;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Lesson;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class LessonService {
    
    @EJB
    private LessonDao lessonDao;
    
    public Lesson addLesson(Lesson lesson){
        return lessonDao.addLesson(lesson);
    }
    
    public void removeLesson(Lesson lesson){
        lessonDao.removeLesson(lesson);
    }
    
    public Lesson getLessonById(Long id){
        return lessonDao.getLessonById(id);
    }
    
    public List<Lesson> getAllLessonByCourse(Course course){
        return lessonDao.getAllLessonByCourse(course);
    }
}
