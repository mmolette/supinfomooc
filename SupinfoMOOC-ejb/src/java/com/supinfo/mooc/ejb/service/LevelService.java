/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.LevelDao;
import com.supinfo.mooc.ejb.entity.Level;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class LevelService {
    
    @EJB
    private LevelDao levelDao;
    
    public Level addLevel(Level level){
        return levelDao.addLevel(level);
    }
    
    public void removeLevel(Level level){
        levelDao.removeLevel(level);
    }
    
    public List<Level> getAllLevels(){
        return levelDao.getAllLevels();
    }
}
