/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.QuestionDao;
import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.entity.Quizz;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Mathieu
 */
@Stateless
public class QuestionService {
    
    @EJB
    private QuestionDao questionDao;
    
    public Question addQuestion(Question question){
        return questionDao.addQuestion(question);
    }
    
    public void removeQuestion(Question question){
        questionDao.removeQuestion(question);
    }
    
    public List<Question> getAllQuestionsByQuizz(Quizz quizz){
        return questionDao.getAllQuestionsByQuizz(quizz);
    }
    
}
