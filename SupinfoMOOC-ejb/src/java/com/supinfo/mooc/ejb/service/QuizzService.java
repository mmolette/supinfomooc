/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.QuizzDao;
import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.entity.Quizz;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Mathieu
 */
@Stateless
public class QuizzService {
    
    @EJB
    private QuizzDao quizzDao;
    
    public Quizz addQuizz(Quizz quizz){
        return quizzDao.addQuizz(quizz);
    }
    
    public void removeQuizz(Quizz quizz){
        quizzDao.removeQuizz(quizz);
    }
    
    public List<Quizz> getAllQuizzByCourse(Course course){
        return quizzDao.getAllQuizzByCourse(course);
    }
    
    public Quizz getQuizzById(Long id){
        return quizzDao.getQuizzById(id);
    }
    
    public List<Answer> getResultQuizz(Quizz quizz, HttpServletRequest req){
        
        List<Answer> goodAnswers = new ArrayList<Answer>();
        
        for(Question q : quizz.getQuestions()){
            Long answerId = Long.valueOf(req.getParameter(String.valueOf(q.getId())));
            for(Answer a : q.getAnswers()){
                if(a.getId() == answerId){
                    if(a.isValid()){
                        goodAnswers.add(a);
                    }
                }
            }
        }
        return goodAnswers;
    }
}
