/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.supinfo.mooc.ejb.service;

import com.supinfo.mooc.ejb.dao.UserDao;
import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.util.Email;
import com.supinfo.mooc.util.Hash;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.persistence.PersistenceException;

/**
 *
 * @author Mathieu
 */
@Stateless
public class UserService {
    
    @EJB 
    private UserDao userDao;
    
    @EJB
    CertificationService certificationService;
    
            
    public User addUser(User user) throws Exception{
        User u;
        try{
            u = userDao.addUser(user);
        }
        catch(Exception e){
            throw e;
        }
        return u;
    }
    
    public void removeUser(User user){
        userDao.removeUser(user);
    }
    
    public User getUserByID(Long id){
        return userDao.getUserByID(id);
    }
    
    /*
    public User getUserByToken(String  token){
        return userDao.getUserByToken(token);
    }
    */
    public User userExists(String email, String password) throws Exception{
        
        try{
            User user = userDao.getUserByEmail(email);
            Hash hash = new Hash();
            password = hash.hash("SHA1", password);

            if(user.getPassword().compareTo(password) != 0){
                user = null;
            }

            return user;
        }catch(Exception e){
            throw e;
        }
    }
    
    
    public Student studentExists(String email, String password) throws Exception{
        
        try{
            Student student = (Student) userDao.getUserByEmail(email);
            Hash hash = new Hash();
            password = hash.hash("SHA1", password);

            if(student.getPassword().compareTo(password) != 0){
                student = null;
            }

            return student;
        }catch(Exception e){
            throw e;
        }
    }
    /*
    public Student studentExists(String email, String password) throws Exception{
        
        try{
            Student student = (Student) userDao.getUserByEmail(email);
            Hash hash = new Hash();
            password = hash.hash("SHA1", password);

            if(student.getPassword().compareTo(password) != 0){
                student = null;
            }

            return student;
        }catch(Exception e){
            throw e;
        }
    }
    */
    public User mergeUser(User user){
        
        return userDao.setUser(user);
    }
    
    public void sendRegistrationMail(User user){
        
        try {
            Email email = new Email();
            String[] to = {user.getEmail()};	

            email.createEmailMessage(to,
                            "Confirmation d'inscription Supinfo MOOC",
                            "Cliquez sur le lien suivant pour activer votre compte: </ br><a href='http://localhost:8080/SupinfoMOOC-war/register?id="
                            + user.getId() + "'>confirmation d'inscription</a>");

            email.sendEmail();

        } catch (AddressException e) {

                e.printStackTrace();
        } catch (MessagingException e) {

                e.printStackTrace();
        }
    }
    
    public void addCertification(Student student, Course course){
        
        Certification certification = new Certification();
        certification.setStudent(student);
        certification.setCourse(course);

        student = certification.getStudent();
        //student.addCertification(certification);
        certificationService.addCertification(certification);
        this.mergeUser(student);
    }
}
