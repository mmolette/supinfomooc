package com.supinfo.mooc.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class Hash {
	
	MessageDigest md;
	
	public Hash(){};
	
	public String hash(String algorithmName, String text){
		
		try {
			md = MessageDigest.getInstance(algorithmName);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Algorithme de hash inconnu");
		}

		md.update(text.getBytes());
		byte[] hash = md.digest();

		return byteArraytoHex(hash);
	}
	
	
	private static String byteArraytoHex(byte[] hash) {
		Formatter formatter = new Formatter();
		String result;
		for (byte b : hash) {
			formatter.format("%02x", b);
		}
		
		result = formatter.toString();
		formatter.close();
		return result;
	}
}
