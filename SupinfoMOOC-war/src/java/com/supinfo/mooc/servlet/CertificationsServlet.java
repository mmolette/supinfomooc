
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.service.CertificationService;
import java.io.IOException;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "CertificationServlet", urlPatterns = {"/auth/certifications"})
public class CertificationsServlet extends HttpServlet {

    @EJB
    CertificationService certificationService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        
        Student student = (Student) req.getSession().getAttribute("user");
        
        req.setAttribute("user", student);
        req.getRequestDispatcher("/jsp/auth/certifications.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
    }

}
