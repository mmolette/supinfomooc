
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.service.CourseService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "CourseServlet", urlPatterns = {"/auth/course"})
public class CourseServlet extends HttpServlet {
    
    @EJB
    CourseService courseService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        Student student = (Student) req.getSession().getAttribute("user");
        Long id = Long.valueOf(req.getParameter("id"));
        
        Course course = courseService.getCourseById(id);
        
        if(course != null && student.getCourses().contains(course)){

            boolean certified = false;
            int i = 0;
            Certification certification = null;
            while(!certified && i < student.getCertifications().size()){
                certification = student.getCertifications().get(i);
                if(certification.getCourse().equals(course)){
                    certified = true;
                }
                i++;
            }

            req.setAttribute("certified", certified);
            req.setAttribute("course", course);
        }
        
        
        
        req.getRequestDispatcher("/jsp/auth/course.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
