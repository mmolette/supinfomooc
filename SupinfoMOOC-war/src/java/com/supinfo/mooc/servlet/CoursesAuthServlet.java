
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Author;
import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.service.CourseService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "CoursesAuthServlet", urlPatterns = {"/auth/courses"})
public class CoursesAuthServlet extends HttpServlet {


    @EJB 
    CourseService courseService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {


        List<Course> courses = courseService.getAllCourses();
        
        Object user = req.getSession().getAttribute("user");
        List<Course> userCourses = new ArrayList<Course>();
        if(req.getSession().getAttribute("user") instanceof Student){
            userCourses = (List<Course>) ((Student) user).getCourses(); 
        }else{
            userCourses = (List<Course>) ((Author) user).getCourses(); 
        }
            
        HashMap<Course, Boolean> coursesMap = new HashMap<>();
        for (Course course : courses) {
                
            //if(user.getCourses().contains(course)){
            if(userCourses.contains(course)){
                coursesMap.put(course, Boolean.TRUE);
            }else{
                coursesMap.put(course, Boolean.FALSE);
            }
            
        }
        
        req.setAttribute("courses", coursesMap);
        req.setAttribute("user", user);
        
        req.getRequestDispatcher("/jsp/auth/courses.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }


}
