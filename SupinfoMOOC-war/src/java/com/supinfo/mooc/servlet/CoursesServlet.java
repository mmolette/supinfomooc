
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.service.CourseService;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "CoursesServlet", urlPatterns = {"/courses"})
public class CoursesServlet extends HttpServlet {
    
    @EJB 
    CourseService courseService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {


        List<Course> courses = courseService.getAllCourses();
        
        req.setAttribute("courses", courses);
        
        req.getRequestDispatcher("jsp/public/courses.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
