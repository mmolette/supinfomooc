
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.service.CourseService;
import com.supinfo.mooc.ejb.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "FollowCourseServlet", urlPatterns = {"/auth/follow"})
public class FollowCourseServlet extends HttpServlet {

    @EJB
    CourseService courseService;
    @EJB
    UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        HttpSession session = req.getSession();
        Student student = (Student) session.getAttribute("user");
        
        Long courseId = Long.valueOf(req.getParameter("id"));
        if(courseId != null){
            Course course = courseService.getCourseById(courseId);
            courseService.followCourse(student, course);
        }
        
        res.sendRedirect(getServletContext().getContextPath()+"/auth/profile");
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


}
