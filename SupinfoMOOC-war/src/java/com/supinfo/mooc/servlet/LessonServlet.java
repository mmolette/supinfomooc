
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Lesson;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.service.LessonService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "LessonServlet", urlPatterns = {"/auth/lesson"})
public class LessonServlet extends HttpServlet {

    @EJB
    LessonService lessonService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        Student student = (Student) req.getSession().getAttribute("user");
        Long id = Long.valueOf(req.getParameter("id"));
        
        Lesson lesson = lessonService.getLessonById(id);
        if(lesson != null && student.getCourses().contains(lesson.getCourse())){
            req.setAttribute("lesson", lesson);
        }
        
        req.getRequestDispatcher("/jsp/auth/lesson.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
