
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.service.UserService;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    
    @EJB
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        if(req.getSession().getAttribute("user") == null){
                req.getRequestDispatcher("jsp/public/login.jsp").forward(req, res);
        }
        else{
            // Affiche le profile de l'utilisateur
            res.sendRedirect(getServletContext().getContextPath()+"/auth/profile");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        try {
            String email = req.getParameter("email").toString();
            String password = req.getParameter("password").toString();
            
            User user = userService.userExists(email, password);
            
            if(user != null){
                
                user.setLastLogin(new Date());
                userService.mergeUser(user);
                
                if(user.isValid()){
                    // Ajoute en session
                    HttpSession session = req.getSession();
                    session.setAttribute("user", user);
                    res.sendRedirect(getServletContext().getContextPath()+"/auth/profile");
                }else{
                    req.setAttribute("account", "disabled");
                    req.getRequestDispatcher("jsp/public/accountValidation.jsp").forward(req, res);
                }
            }else{

            }
        } catch (Exception ex) {
            // Affiche la page d'erreur
            req.setAttribute("error", ex.getMessage());
            req.getRequestDispatcher("jsp/public/messageError.jsp").forward(req, res); 
        }
    }

}
