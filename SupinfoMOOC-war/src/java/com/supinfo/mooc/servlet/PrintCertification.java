
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.service.CertificationService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "PrintCertification", urlPatterns = {"/auth/print"})
public class PrintCertification extends HttpServlet {

    @EJB
    CertificationService certificationService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        Long certifId = Long.valueOf(req.getParameter("id"));
        
        Certification certification = certificationService.getCertificationById(certifId);
        certificationService.printCertification(certification);
        req.getRequestDispatcher("/jsp/auth/printCertification.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }


}
