
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "ProfileServlet", urlPatterns = {"/auth/profile"})
public class ProfileServlet extends HttpServlet {

    @EJB
    UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        User user = (User) req.getSession().getAttribute("user");
        user = userService.getUserByID(user.getId());
        
        req.setAttribute("user", user);
        req.getRequestDispatcher("/jsp/auth/profile.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }


}
