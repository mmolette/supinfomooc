
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.entity.Quizz;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.service.CourseService;
import com.supinfo.mooc.ejb.service.QuizzService;
import com.supinfo.mooc.ejb.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "QuizzServlet", urlPatterns = {"/auth/quizz"})
public class QuizzServlet extends HttpServlet {

    @EJB
    CourseService courseService;
    
    @EJB
    QuizzService quizzService;
    
    @EJB
    UserService userService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        Student student = (Student) req.getSession().getAttribute("user");
        Long courseId = Long.valueOf(req.getParameter("id"));
        
        if(courseId != null){
            Course course = courseService.getCourseById(courseId);
            if(course != null && student.getCourses().contains(course)){
                req.setAttribute("course", course);
            }
        }
        
        req.getRequestDispatcher("/jsp/auth/quizz.jsp").forward(req, res);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        Student student = (Student) req.getSession().getAttribute("user");
        Long id = Long.valueOf(req.getParameter("quizz"));
        
        Quizz quizz = quizzService.getQuizzById(id);
        
        List<Answer> goodAnswers = quizzService.getResultQuizz(quizz, req);
        int nb = goodAnswers.size();
        int total = quizz.getQuestions().size();
        int score = 0;
        
        score = (int)(nb * 100.0 / total);
        if(score > 60){
            Course course = courseService.getCourseById(quizz.getCourse().getId());
            userService.addCertification(student, course);
        }
        
        req.setAttribute("score", score);
        req.setAttribute("user", student);
        req.setAttribute("course", quizz.getCourse());
        req.getRequestDispatcher("/jsp/auth/quizzResult.jsp").forward(req, res);
    }

}
