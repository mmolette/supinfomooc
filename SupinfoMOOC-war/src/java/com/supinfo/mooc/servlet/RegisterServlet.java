
package com.supinfo.mooc.servlet;

import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mathieu
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {
    
    @EJB
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        
        if(req.getSession().getAttribute("user") == null){
            
            if(req.getParameter("id") != null){
				
                Long id = Long.parseLong(req.getParameter("id"));

                User user = userService.getUserByID(id);
                user.setValid(true);

                userService.mergeUser(user);
                
                req.setAttribute("account", "enabled");
                req.getRequestDispatcher("jsp/public/accountValidation.jsp").forward(req, res);

            }else{
                req.getRequestDispatcher("jsp/public/register.jsp").forward(req, res);
            }
			  
        }
        else{
            // Affiche le profile de l'utilisateur
            res.sendRedirect(getServletContext().getContextPath()+"/auth/profile");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        String email = req.getParameter("email").toString();
        String lastname = req.getParameter("lastname").toString();
        String firstname = req.getParameter("firstname").toString();
        String password = req.getParameter("password").toString();
        
        Student student = new Student(email, lastname, firstname, password);
        String message = null;
        
        
        try{
            userService.addUser(student);
        }catch(Exception e){
            if(e.getCause().getMessage().compareTo("EmailDuplicateException") == 0){
                message = e.getMessage();
            }else{
                message = "An error occured while creating your account !";
            }
        }
        
        if(message == null){
           userService.sendRegistrationMail(student);
           // Affiche le profile de l'utilisateur
            req.setAttribute("account", "validation");
            req.getRequestDispatcher("jsp/public/accountValidation.jsp").forward(req, res);
        }else{
            // Affiche la page d'erreur
            req.setAttribute("error", message);
            req.getRequestDispatcher("jsp/public/messageError.jsp").forward(req, res); 
        }   
    }

}
