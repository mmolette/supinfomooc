
package com.supinfo.mooc.soap;

import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.service.AnswerService;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Mathieu
 */
@WebService(serviceName = "AnswerWS")
public class AnswerWS {
    @EJB
    private AnswerService ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "addAnswer")
    public Answer addAnswer(@WebParam(name = "answer") Answer answer) {
        return ejbRef.addAnswer(answer);
    }

    @WebMethod(operationName = "removeAnswer")
    @Oneway
    public void removeAnswer(@WebParam(name = "answer") Answer answer) {
        ejbRef.removeAnswer(answer);
    }

    @WebMethod(operationName = "getAnswersByQuestion")
    public List<Answer> getAnswersByQuestion(@WebParam(name = "question") Question question) {
        return ejbRef.getAnswersByQuestion(question);
    }
    
}
