
package com.supinfo.mooc.soap;

import com.supinfo.mooc.ejb.entity.Category;
import com.supinfo.mooc.ejb.service.CategoryService;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Mathieu
 */
@WebService(serviceName = "CategoryWS")
public class CategoryWS {
    
    @EJB
    private CategoryService ejbRef;

    @WebMethod(operationName = "addCategory")
    public Category addCategory(@WebParam(name = "category") Category category) {
        return ejbRef.addCategory(category);
    }

    @WebMethod(operationName = "removeCategory")
    @Oneway
    public void removeCategory(@WebParam(name = "category") Category category) {
        ejbRef.removeCategory(category);
    }

    @WebMethod(operationName = "getAllCategories")
    public List<Category> getAllCategories() {
        return ejbRef.getAllCategories();
    }
    
}
