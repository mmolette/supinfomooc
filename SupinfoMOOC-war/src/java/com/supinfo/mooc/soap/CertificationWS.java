
package com.supinfo.mooc.soap;

import com.supinfo.mooc.ejb.entity.Certification;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.service.CertificationService;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Mathieu
 */
@WebService(serviceName = "CertificationWS")
public class CertificationWS {
    
    @EJB
    private CertificationService ejbRef;

    @WebMethod(operationName = "addCertification")
    public Certification addCertification(@WebParam(name = "certification") Certification certification) {
        return ejbRef.addCertification(certification);
    }

    @WebMethod(operationName = "removeCertification")
    @Oneway
    public void removeCertification(@WebParam(name = "certification") Certification certification) {
        ejbRef.removeCertification(certification);
    }

    @WebMethod(operationName = "getCertificationById")
    public Certification getCertificationById(@WebParam(name = "id") Long id) {
        return ejbRef.getCertificationById(id);
    }

    @WebMethod(operationName = "getCertificationsByStudent")
    public List<Certification> getCertificationsByStudent(@WebParam(name = "student") Student student) {
        return ejbRef.getCertificationsByStudent(student);
    }

    @WebMethod(operationName = "printCertification")
    @Oneway
    public void printCertification(@WebParam(name = "certification") Long id) {
        
        Certification certification = ejbRef.getCertificationById(id);
        ejbRef.printCertification(certification);
    }
    
}
