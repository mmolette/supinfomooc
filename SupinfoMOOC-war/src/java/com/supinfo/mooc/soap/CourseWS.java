
package com.supinfo.mooc.soap;

import com.supinfo.mooc.ejb.entity.Category;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Level;
import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.service.CourseService;
import com.supinfo.mooc.ejb.service.UserService;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Mathieu
 */
@WebService(serviceName = "CourseWS")
public class CourseWS {
    
    @EJB
    private CourseService ejbRef;
    @EJB
    private UserService userService;

    @WebMethod(operationName = "addCourse")
    public Course addCourse(@WebParam(name = "course") Course course) {
        return ejbRef.addCourse(course);
    }

    @WebMethod(operationName = "removeCourse")
    @Oneway
    public void removeCourse(@WebParam(name = "course") Course course) {
        ejbRef.removeCourse(course);
    }

    @WebMethod(operationName = "getAllCourses")
    public List<Course> getAllCourses() {
        return ejbRef.getAllCourses();
    }

    @WebMethod(operationName = "getCourseById")
    public Course getCourseById(@WebParam(name = "id") Long id) {
        return ejbRef.getCourseById(id);
    }

    @WebMethod(operationName = "getAllCoursesByUser")
    public List<Course> getAllCoursesByUser(@WebParam(name = "user") User user) {
        return ejbRef.getAllCoursesByUser(user);
    }

    @WebMethod(operationName = "getAllCoursesByLevel")
    public List<Course> getAllCoursesByLevel(@WebParam(name = "level") Level level) {
        return ejbRef.getAllCoursesByLevel(level);
    }

    @WebMethod(operationName = "getAllCoursesByCategory")
    public List<Course> getAllCoursesByCategory(@WebParam(name = "category") Category category) {
        return ejbRef.getAllCoursesByCategory(category);
    }

    @WebMethod(operationName = "getAllCoursesByCategoryAndLevel")
    public List<Course> getAllCoursesByCategoryAndLevel(@WebParam(name = "category") Category category, @WebParam(name = "level") Level level) {
        return ejbRef.getAllCoursesByCategoryAndLevel(category, level);
    }

    @WebMethod(operationName = "followCourse")
    @Oneway
    public void followCourse(@WebParam(name = "studentId") Long studentId, @WebParam(name = "courseId") Long courseId) {
        
        Student student = (Student) userService.getUserByID(studentId);
        Course course = ejbRef.getCourseById(courseId);
        ejbRef.followCourse(student, course);
    }
    
}
