
package com.supinfo.mooc.soap;

import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Lesson;
import com.supinfo.mooc.ejb.service.LessonService;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Mathieu
 */
@WebService(serviceName = "LessonWS")
public class LessonWS {
    
    @EJB
    private LessonService ejbRef;

    @WebMethod(operationName = "addLesson")
    public Lesson addLesson(@WebParam(name = "lesson") Lesson lesson) {
        return ejbRef.addLesson(lesson);
    }

    @WebMethod(operationName = "removeLesson")
    @Oneway
    public void removeLesson(@WebParam(name = "lesson") Lesson lesson) {
        ejbRef.removeLesson(lesson);
    }

    @WebMethod(operationName = "getLessonById")
    public Lesson getLessonById(@WebParam(name = "id") Long id) {
        return ejbRef.getLessonById(id);
    }

    @WebMethod(operationName = "getAllLessonByCourse")
    public List<Lesson> getAllLessonByCourse(@WebParam(name = "course") Course course) {
        return ejbRef.getAllLessonByCourse(course);
    }
    
}
