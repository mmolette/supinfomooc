
package com.supinfo.mooc.soap;

import com.supinfo.mooc.ejb.entity.Question;
import com.supinfo.mooc.ejb.entity.Quizz;
import com.supinfo.mooc.ejb.service.QuestionService;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Mathieu
 */
@WebService(serviceName = "QuestionWS")
public class QuestionWS {
    
    @EJB
    private QuestionService ejbRef;

    @WebMethod(operationName = "addQuestion")
    public Question addQuestion(@WebParam(name = "question") Question question) {
        return ejbRef.addQuestion(question);
    }

    @WebMethod(operationName = "removeQuestion")
    @Oneway
    public void removeQuestion(@WebParam(name = "question") Question question) {
        ejbRef.removeQuestion(question);
    }

    @WebMethod(operationName = "getAllQuestionsByQuizz")
    public List<Question> getAllQuestionsByQuizz(@WebParam(name = "quizz") Quizz quizz) {
        return ejbRef.getAllQuestionsByQuizz(quizz);
    }
    
}
