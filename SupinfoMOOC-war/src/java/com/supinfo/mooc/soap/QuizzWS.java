
package com.supinfo.mooc.soap;

import com.supinfo.mooc.ejb.entity.Answer;
import com.supinfo.mooc.ejb.entity.Course;
import com.supinfo.mooc.ejb.entity.Quizz;
import com.supinfo.mooc.ejb.service.QuizzService;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Mathieu
 */
@WebService(serviceName = "QuizzWS")
public class QuizzWS {
    @EJB
    private QuizzService ejbRef;

    @WebMethod(operationName = "addQuizz")
    public Quizz addQuizz(@WebParam(name = "quizz") Quizz quizz) {
        return ejbRef.addQuizz(quizz);
    }

    @WebMethod(operationName = "removeQuizz")
    @Oneway
    public void removeQuizz(@WebParam(name = "quizz") Quizz quizz) {
        ejbRef.removeQuizz(quizz);
    }

    @WebMethod(operationName = "getAllQuizzByCourse")
    public List<Quizz> getAllQuizzByCourse(@WebParam(name = "course") Course course) {
        return ejbRef.getAllQuizzByCourse(course);
    }

    @WebMethod(operationName = "getQuizzById")
    public Quizz getQuizzById(@WebParam(name = "id") Long id) {
        return ejbRef.getQuizzById(id);
    }
    
}
