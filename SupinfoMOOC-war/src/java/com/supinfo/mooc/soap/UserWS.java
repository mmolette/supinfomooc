
package com.supinfo.mooc.soap;

import com.supinfo.mooc.ejb.entity.Student;
import com.supinfo.mooc.ejb.entity.User;
import com.supinfo.mooc.ejb.service.UserService;
import java.sql.Timestamp;
import java.util.Date;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author Mathieu
 */
@WebService(serviceName = "UserWS")
public class UserWS {
    
    @EJB
    private UserService ejbRef;

    @WebMethod(operationName = "addUser")
    public User addUser(@WebParam(name = "user") User user) throws Exception {
        return ejbRef.addUser(user);
    }

    @WebMethod(operationName = "removeUser")
    @Oneway
    public void removeUser(@WebParam(name = "user") User user) {
        ejbRef.removeUser(user);
    }

    @WebMethod(operationName = "getUserByID")
    public User getUserByID(@WebParam(name = "id") Long id) {
        return ejbRef.getUserByID(id);
    }

    @WebMethod(operationName = "authenticate")
    public String userExists(@WebParam(name = "email") String email, @WebParam(name = "password") String password) throws Exception {
        if(ejbRef.userExists(email, password) != null){
            Date date = new Date();
            return (new Timestamp(date.getTime()).toString());
        }
        return null;
    }

    @WebMethod(operationName = "studentExists")
    public Student studentExists(@WebParam(name = "email") String email, @WebParam(name = "password") String password) throws Exception {
        return ejbRef.studentExists(email, password);
    }

    @WebMethod(operationName = "mergeUser")
    public User mergeUser(@WebParam(name = "user") User user) {
        return ejbRef.mergeUser(user);
    }
    
}
