<%-- 
    Document   : index.jsp
    Created on : 26 févr. 2014, 14:28:47
    Author     : Mathieu
--%>
<%@page import="com.supinfo.mooc.ejb.dao.UserDao"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel=stylesheet type="text/css" href="../css/style.css">
        <title>Profile</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        
        <h2>Your certification(s) :</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Course</th>
                    <th>Description</th>
                    <th>Author</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${user.certifications}" var="certif">
                    <tr>
                        <td>${certif.course.name}</td>
                        <td>${certif.course.description}</td>
                        <td>${certif.course.author.lastname} ${certif.course.author.firstname}</td>
                        <td>${certif.date}</td>
                        <td>
                            <a href="print?id=${certif.id}" class='btn btn-sm btn-success'>Print</a>
                        </td>
                    </tr>
                </c:forEach> 
            </tbody>
        </table>
        
        
        <%@ include file="../template/footer.jsp" %>
    </body>
</html>
