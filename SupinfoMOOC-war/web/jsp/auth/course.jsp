<%-- 
    Document   : index.jsp
    Created on : 26 févr. 2014, 14:28:47
    Author     : Mathieu
--%>
<%@page import="com.supinfo.mooc.ejb.dao.UserDao"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel=stylesheet type="text/css" href="../css/style.css">
        <title>Course - ${course.name}</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        
        <c:choose>
            <c:when test="${empty course}">You don't have access rights to this course !</c:when>
            <c:otherwise>
                <h2>Course : ${course.name}</h2>
                <h4>${course.author.lastname} ${course.author.firstname}</h4>

                Category : ${course.category.name}<br />
                Level : ${course.level.name}<br />
                Duration : ${course.duration}<br />
                Description : ${course.description}<br />

                <h3>Lesson(s) :</h3>
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Duration</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${course.lessons}" var="lesson">
                            <tr>
                                <td>${lesson.name}</td>
                                <td>${lesson.description}</td>
                                <td>${lesson.duration} min.</td>
                                <td>
                                    <a href="lesson?id=${lesson.id}" class='btn btn-sm btn-success'>Go !</a>
                                </td>
                            </tr>
                        </c:forEach> 

                    </tbody>
                </table>


                <br /><br />

                <h3>Quizz :</h3>
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Duration</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:if test="${!empty course.quizz}">
                            <tr>
                                <td>${course.quizz.name}</td>
                                <td>${course.quizz.description}</td>
                                <td>${course.quizz.duration} min.</td>
                                <td>
                                    <c:choose>
                                        <c:when test="${certified}">
                                            <span class="label label-success">Certified</span>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="quizz?id=${course.quizz.id}" class='btn btn-sm btn-success'>Go !</a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:if>
                    </tbody>
                </table>
            </c:otherwise>
        </c:choose>
        
        
        
        <%@ include file="../template/footer.jsp" %>
    </body>
</html>
