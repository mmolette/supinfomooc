<%-- 
    Document   : index.jsp
    Created on : 26 févr. 2014, 14:28:47
    Author     : Mathieu
--%>
<%@page import="com.supinfo.mooc.ejb.dao.UserDao"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel=stylesheet type="text/css" href="../css/style.css">
        <title>Lesson - ${lesson.name}</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        
        <c:choose>
            <c:when test="${empty lesson}">You don't have access rights to this course !</c:when>
            <c:otherwise>
                <h2>${lesson.course.name}</h2>
        
                <h3>${lesson.name} :</h3>

                Description : ${lesson.description}

                <br /><br />

                ${lesson.content}
            </c:otherwise>
        </c:choose>
        
        
        <%@ include file="../template/footer.jsp" %>
    </body>
</html>
