<%-- 
    Document   : index.jsp
    Created on : 26 févr. 2014, 14:28:47
    Author     : Mathieu
--%>
<%@page import="com.supinfo.mooc.ejb.dao.UserDao"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel=stylesheet type="text/css" href="../css/style.css">
        <title>Profile</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        <h2>Your profile :</h2>
        <ul>
            <li>Lastname : <c:out value="${user.lastname}"/></li>
            <li>Firstname : <c:out value="${user.firstname}"/></li>
            <li>Email : <c:out value="${user.email}"/></li>
            <li>Registered : <c:out value="${user.registered}"/></li>
        </ul>
        
        <h2>Your course(s) :</h2>
            <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Duration</th>
                    <th>Author</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${user.courses}" var="course">
                    <tr>
                        <td>${course.name}</td>
                        <td>${course.description}</td>
                        <td>${course.duration} min.</td>
                        <td>${course.author.lastname} ${course.author.firstname}</td>
                        <td>
                            <a href="course?id=${course.id}" class='btn btn-sm btn-success'>Go !</a>
                        </td>
                    </tr>
                </c:forEach> 
            </tbody>
        </table>
        
        
        <%@ include file="../template/footer.jsp" %>
    </body>
</html>
