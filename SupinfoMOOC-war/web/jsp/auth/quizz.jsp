<%-- 
    Document   : index.jsp
    Created on : 26 févr. 2014, 14:28:47
    Author     : Mathieu
--%>
<%@page import="com.supinfo.mooc.ejb.dao.UserDao"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel=stylesheet type="text/css" href="../css/style.css">
        <title>Quizz - ${course.name}</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        <c:choose>
            <c:when test="${empty course}">You don't have access rights to this course !</c:when>
            <c:otherwise>
                <h2>Quizz : ${course.quizz.name}</h2>
        
                ${course.quizz.description}<br /><br />

                <form method="POST" action="quizz">
                    <input type="text" name="quizz" value="${course.quizz.id}" style="display: none" />
                    <ul>
                        <c:forEach items="${course.quizz.questions}" var="q">
                            ${counter.count}
                            <li>${q.name} :
                                <ul>
                                    <c:forEach items="${q.answers}" var="a" varStatus="counter">
                                        <c:choose>
                                            <c:when test="${counter.count == 1}">
                                                <input type="radio" name="${q.id}" value="${a.id}" checked="true">${a.name}
                                            </c:when>
                                            <c:otherwise>
                                                <input type="radio" name="${q.id}" value="${a.id}">${a.name}
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </ul>
                            </li>
                        </c:forEach>  
                    </ul>
                    <input type="submit" value="Submit" class="btn btn-lg btn-success" />
                </form>
            </c:otherwise>
        </c:choose>
        
        
        
        
        <%@ include file="../template/footer.jsp" %>
    </body>
</html>
