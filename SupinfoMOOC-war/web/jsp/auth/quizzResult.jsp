<%-- 
    Document   : index.jsp
    Created on : 26 févr. 2014, 14:28:47
    Author     : Mathieu
--%>
<%@page import="com.supinfo.mooc.ejb.dao.UserDao"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="../jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="../bootstrap/css/bootstrap.min.css">
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
        <link rel=stylesheet type="text/css" href="../css/style.css">
        <title>Quizz result- ${course.name}</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        
        <h2>Quizz result: ${course.quizz.name}</h2>
        <c:choose>
            <c:when test="${score >= 60}">
                Congratulations ${user.lastname} ${user.firstname} your score is ${score} % !<br />
                
                You get a new certification for ${course.name}.
            </c:when>
            <c:otherwise>
                Sorry you failed.<br />
                Your score is ${score}, you must have at least 60%.
            </c:otherwise>
        </c:choose>
            
        <%@ include file="../template/footer.jsp" %>
    </body>
</html>
