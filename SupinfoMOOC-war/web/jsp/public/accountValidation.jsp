<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <script type="text/javascript" src="jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel=stylesheet type="text/css" href="css/style.css">
        <title>Account validation</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        
        <c:choose>
           <c:when test="${account == 'enabled'}"><h1>Your account has been enabled successfully !</h1></c:when>
           <c:when test="${account == 'disabled'}"><h1>Your account is not enable ! Check your mailbox.</h1></c:when>
           <c:when test="${account == 'validation'}"><h1>An email was sent to enable your account !</h1><br />
               <a href="login" class="btn btn-sm btn-success">Login</a>
           </c:when>
        </c:choose>   
        
        <%@ include file="../template/footer.jsp" %>
    </body>
</html>
