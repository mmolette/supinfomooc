<%-- 
    Document   : index.jsp
    Created on : 26 févr. 2014, 14:28:47
    Author     : Mathieu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <script type="text/javascript" src="jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="bootstrap/css/bootstrap.min.css">
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <link rel=stylesheet type="text/css" href="css/style.css">
 
        <title>Courses</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        
        <h1>Courses :</h1>
        <!--
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Duration</th>
                    <th>Author</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${courses}" var="course">
                    <tr>
                        <td>${course.name}</td>
                        <td>${course.description}</td>
                        <td>${course.duration} min.</td>
                        <td>${course.author.lastname} ${course.author.firstname}</td>
                    </tr>
                </c:forEach> 
            </tbody>
        </table>
        -->
        <div class="panel-group" id="accordion" style="margin: 10px auto; width: 98%;">
            <c:forEach items="${courses}" var="course" varStatus="status">
                <c:if test="${status.count == 1}"><div style="display: block; width: 100%; overflow: auto;"></c:if>
                <div class="panel panel-default inline-block pull-left" style="margin: 5px 5px; width: 32%;">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#${course.id}">
                        [${course.category.name}] ${course.name}
                      </a>
                    </h4>
                  </div>
                  <div id="${course.id}" class="panel-collapse collapse in">
                    <div class="panel-body">
                        Author : ${course.author.lastname} ${course.author.firstname}<br />
                        Level : ${course.level.name}<br />
                        Duration : ${course.duration} min.<br />
                        Description : ${course.description}<br/>
                        Lessons :
                        <ul>
                            <c:forEach items="${course.lessons}" var="lesson">
                                <li>${lesson.name}</li>
                            </c:forEach>
                        </ul>
                    </div>
                  </div>
                </div>
                <c:choose>
                    <c:when test="${status.count % 3 == 0}"></div><div style="display: block; width: 100%;"></c:when>
                    <c:when test="${status.count == fn:length(courses)}"></div></c:when>
                </c:choose>
            </c:forEach>
          </div>

        
        <%@ include file="../template/footer.jsp" %>
        
        <script type="text/javascript">
            $(function(){
                $('.collapse').collapse();
            })
        </script>
    </body>
</html>
