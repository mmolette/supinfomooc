<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <script type="text/javascript" src="jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel=stylesheet type="text/css" href="css/style.css">
        <title>Account validation</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        
        <h1>An error occured !</h1>
        <p><c:out value="${error}"></c:out></p>
        
        <%@ include file="../template/footer.jsp" %>
    </body>
</html>
