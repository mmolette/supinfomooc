<%-- 
    Document   : register
    Created on : 26 févr. 2014, 14:20:04
    Author     : Geoffrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <script type="text/javascript" src="jquery/jquery.js"></script>
        
        <link rel=stylesheet type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel=stylesheet type="text/css" href="css/style.css">
        <title>Register</title>
    </head>
    <body>
        <%@ include file="../template/header.jsp" %>
        
        <form id="loginForm" role="form" action="register" method="post">
            <div class="form-group">
              <input name="firstname" type="text" placeholder='Firstname' class="form-control" required>
            </div>
            <div class="form-group">
              <input name="lastname" type="text" placeholder='Lastname' class="form-control" required>
            </div>
            <div class="form-group">
              <input name="email" type="email" placeholder='Email' class="form-control" required>
            </div>
            <div class="form-group">
              <input name="password" type="password" placeholder='Password' class="form-control" required>
            </div>
            <div class="form-group">
              <input type="submit" value="Sign up !" class="form-control">
            </div>
            <p>Not a member? <a href="register">Sign up now</a></p>
        </form>
        
        <%@ include file="../template/footer.jsp" %>
    </body>
    
</html>
