<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--<nav>
    <c:choose>
        <c:when test="${empty user}">
            <ul>
                <li id="title"><a href="login">supmooc</a></li>
                <li><a href="courses">courses</a></li>
                <li id="li_right"><a href="login">Log in</a></li>
            </ul>
        </c:when>
        <c:otherwise>
            <ul>
                <li><a href="profile">profile</a></li>
                <li><a href="courses">courses</a></li>
                <li id="li_right"><a href="../logout">Logout</a></li>
            </ul>
        </c:otherwise>
    </c:choose>
</nav>-->
            
<c:choose>
    <c:when test="${empty user}">
        <nav class="navbar navbar-default navbar-fixed-top red navbar-inverse" role="navigation">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li><a href="login">SUPMOOC</a></li>
                  <li><a href="courses">Courses</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="register">Register</a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </c:when>
    <c:otherwise>
        <nav class="navbar navbar-default navbar-fixed-top red navbar-inverse" role="navigation">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li><a href="profile">Profile</a></li>
                  <c:if test="${user.getClass().name == 'com.supinfo.mooc.ejb.entity.Student'}">
                        <li><a href="courses">Courses</a></li>
                        <li><a href="certifications">My certifications</a></li>
                  </c:if>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="../logout">Logout</a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </c:otherwise>
</c:choose>
            
            
